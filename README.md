# Vagrant, Docker and php

## to use this

1. clone this repo
2. change the config to your setup (see below)
3. install docker and vagrant (>1.6)
4. in the root run `vagrant up --provider docker`
5. go and grab a coffee the first time
6. later, go to http://localhost:9876, voilà!
7. you can edit your php stuff live in the www folder

## /!\ Proxy

If you are behind a proxy, there are 2 things to configure.

Assuming you have some proxy (like cntlm) on your machine, you must:

- configure the proxy in the `/etc/default/docker` file with localhost and restart docker.
- configure apt in the container `apt/apt.conf` with your ip
- if you plan tu use `composer.phar`, you'll also need to provide the proxy in the `Dockerfile` (see the commented line)


## Logs

Logs (nginx, php-fpm) appear in the `run/log` folder.

## Normal operations

### Starting

    vagrant up --provider docker

### Stoping

    vagrant [suspend|destroy]

Use the second one if you change some configuration.

### a shell on the container

    vagrant -t -i <name of the image> /bin/bash

### listing images

    docker images

If you don't commit a docker container, it will have generated name.

Example output

    <none>              <none>              d7ad64b3430b        About an hour ago   245.3 MB
    <none>              <none>              f11338006769        About an hour ago   241 MB
    <none>              <none>              3b57ade6f136        2 hours ago         241.3 MB
    debian              latest              25daec02219d        11 days ago         85.18 MB
    debian              wheezy              25daec02219d        11 days ago         85.18 MB
    
I would try d7ad64b3430b.

### clean up

You shouldn't do this if you don't play a lot with docker:

    # Delete all containers
    docker rm $(docker ps -a -q)
    # Delete all images
    docker rmi $(docker images -q)

## Modifications

### apt

You can add packages to the container in the `Docker/DockerFile`.

### http

You can change the default http port in the VagrantFile.

### php composer

composer.phar is included in the `php` folder. You can add you project dependencies in the `composer.json` file. For your changes to work, you must restart the vagrant.

Don't put the `composer.json` file in the www directory, it will not work.


## TODO

Create another Vagrant/Docker for storage (whatever is needed) or add it on this setup (still thinking about it).
